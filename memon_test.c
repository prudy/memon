/* 
 * Copyright (C) 2008 Przemek Rudy
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const char *str = "1234567890";

extern void Sub1(void *toFree);

int main(void)
{
    void *ptr1, *ptr2, *ptr3, *ptr4, *ptr5;
    
    printf("Test begin\n");
    ptr1 = malloc(1);
    ptr2 = malloc(1);
    ptr3 = malloc(1);
    ptr4 = strdup(str);
    ptr5 = strndup(str, 4);
    
    Sub1(ptr1);
    
    ptr3 = realloc(ptr3, 20);
    
    free(ptr2);
    free(ptr3);
    
    free(ptr5);
    free(ptr4);
    
    printf("Test end\n");
    return 0;
}
