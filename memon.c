/* 
 * Copyright (C) 2008 Przemek Rudy
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MEMON_PREFIX
#define MEMON_PREFIX ""
#endif

#ifndef MEMON_HISTORY_MAX
#define MEMON_HISTORY_MAX 500
#endif

#ifndef MEMON_DUMP_MAX
#define MEMON_DUMP_MAX 60
#endif

#ifndef MEMON_NO_COLORS
#define CDEF   "\x1B[0m"
#define CRED   "\x1B[31m"
#define CGRN   "\x1B[32m"
#define CYEL   "\x1B[33m"
#define CBLU   "\x1B[34m"
#define CMAG   "\x1B[35m"
#define CCYN   "\x1B[36m"
#define CWHT   "\x1B[37m"
static const char *colors[] = {CRED, CGRN, CYEL, CBLU, CMAG, CCYN, CWHT};
#define MEMON_DUMP_BUF_SIZE ((MEMON_DUMP_MAX) * (1+8) + 8)
#else
#define MEMON_DUMP_BUF_SIZE MEMON_DUMP_MAX
#endif


static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static int historyLevel = 0;
static int allocations = 0;
static int missed = 0;
#ifndef MEMON_NO_ADDRESS
static void *handles[MEMON_HISTORY_MAX+1];
#else
static void *handles[MEMON_HISTORY_MAX];
#endif

void *__real_malloc(size_t size);
void __real_free(void *ptr);
char *__real_strdup(const char *s);
char *__real_strndup(const char *s, size_t n);
char *__real_realloc(const char *s, size_t n);

static int AddAllocation(void *ptr)
{
    int i, pos=-1;
    
    if(ptr != NULL)
    {
	if(historyLevel < MEMON_HISTORY_MAX)
	{
	    for(i = 0; i < MEMON_HISTORY_MAX; ++i)
	    {
		if(handles[i] == NULL)
		{
		    pos = i;
		    handles[i] = ptr;
		    if(i >= historyLevel)
		    {
			++historyLevel;
		    }
		    ++allocations;
		    break;
		}
	    }
	}
    }
    return pos;
}

static int RemoveAllocation(void *ptr)
{
    int i, pos=-1;
    
    if(ptr != NULL)
    {
	if(historyLevel < MEMON_HISTORY_MAX)
	{
	    ++missed;
	    for(i = 0; i < historyLevel; ++i)
	    {
		if(handles[i] == ptr)
		{
		    pos = i;
#ifndef MEMON_NO_ADDRESS
		    handles[MEMON_HISTORY_MAX] = ptr;
#endif
		    handles[i] = NULL;
		    if(i == historyLevel - 1)
		    {
			for(; (i >= 0) && (handles[i] == NULL); --i);
			historyLevel = i + 1;
		    }
		    --allocations;
		    --missed;
		}
	    }
	}
    }
    return pos;
}

static int ReplaceAllocation(void *old, void *ptr)
{
    int i, pos=-1;
    
    if((ptr != NULL) && (old != NULL))
    {
	if(historyLevel < MEMON_HISTORY_MAX)
	{
	    for(i = 0; i < MEMON_HISTORY_MAX; ++i)
	    {
		if(handles[i] == old)
		{
		    pos = i;
		    handles[i] = ptr;
		    break;
		}
	    }
	}
    }
    else if((old == NULL) && (ptr != NULL))
    {
	pos = AddAllocation(ptr);
    }
    else if((old != NULL) && (ptr == NULL))
    {
	pos = RemoveAllocation(old);
    }
    return pos;
}

#ifndef MEMON_NO_COLORS
static void Dump(char *fnc, int pos)
{
    int i, p, offset, level;
    char buf[MEMON_DUMP_BUF_SIZE + 1];

    if(historyLevel < MEMON_HISTORY_MAX)
    {
	level = pos >= historyLevel ? pos+1 : historyLevel;
	offset = level > MEMON_DUMP_MAX ? level - MEMON_DUMP_MAX : 0;
	for(i = 0, p = 0; offset < level; ++i, ++p, ++offset)
	{
	    strcpy(buf+p, colors[offset % (sizeof(colors)/sizeof(char *))]);
	    p += strlen(buf+p);
	    buf[p] = pos == offset ? (handles[offset] ? '+' : '-') : (handles[offset] ? '*' : ' ');
	}
	strcpy(buf+p, CDEF);
	p += strlen(CDEF);
	for(; i < MEMON_DUMP_MAX; ++i, ++p)
	{
	    buf[p] = ' ';
	}
	buf[p] = '\0';
#ifndef MEMON_NO_ADDRESS
	printf("%s%-8.8s%s@%-3d%s:%3u %c%s]%d %s%p%s\n", MEMON_PREFIX, fnc, 
	    colors[pos % (sizeof(colors)/sizeof(char *))], pos, CDEF,
	    allocations, level > MEMON_DUMP_MAX ? '<' : '[', buf, missed,
	    colors[pos % (sizeof(colors)/sizeof(char *))], handles[pos] ? handles[pos] : handles[MEMON_HISTORY_MAX], CDEF);
#else
	printf("%s%-8.8s%s@%-3d%s:%3u %c%s]%d\n", MEMON_PREFIX, fnc, 
	    colors[pos % (sizeof(colors)/sizeof(char *))], pos, CDEF,
	    allocations, level > MEMON_DUMP_MAX ? '<' : '[', buf, missed);
#endif
    }
    else
    {
	printf("%s%-8.8s@   #    [ overflow ]\n", MEMON_PREFIX, fnc);
    }
}
#else
static void Dump(char *fnc, int pos)
{
    int i, offset, level;
    char buf[MEMON_DUMP_BUF_SIZE + 1];

    if(historyLevel < MEMON_HISTORY_MAX)
    {
	level = pos >= historyLevel ? pos+1 : historyLevel;
	offset = level > MEMON_DUMP_MAX ? level - MEMON_DUMP_MAX : 0;
	for(i = 0; offset < level; ++i, ++offset)
	{
	    buf[i] = pos == offset ? (handles[offset] ? '+' : '-') : (handles[offset] ? '*' : '.');
	}
	for(; i < MEMON_DUMP_MAX; ++i)
	{
	    buf[i] = ' ';
	}
	buf[i] = '\0';
#ifndef MEMON_NO_ADDRESS
	printf("%s%-8.8s@%-3d:%3u %c%s]%d %p\n", MEMON_PREFIX, fnc, pos, allocations, level > MEMON_DUMP_MAX ? '<' : '[', buf, missed,
	    handles[pos] ? handles[pos] : handles[MEMON_HISTORY_MAX]);
#else
	printf("%s%-8.8s@%-3d:%3u %c%s]%d\n", MEMON_PREFIX, fnc, pos, allocations, level > MEMON_DUMP_MAX ? '<' : '[', buf, missed);
#endif
    }
    else
    {
	printf("%s%-8.8s@   #    [ overflow ]\n", MEMON_PREFIX, fnc);
    }
}
#endif

void *__wrap_malloc(size_t size)
{
    void *ptr;
    int pos;
    
    pthread_mutex_lock(&mutex);
    ptr = __real_malloc(size);
    pos = AddAllocation(ptr);

    Dump("malloc", pos);

    pthread_mutex_unlock(&mutex);
    return ptr;
}

void *__wrap_realloc(void *old, size_t size)
{
    void *ptr;
    int pos;
    
    pthread_mutex_lock(&mutex);
    ptr = __real_realloc(old, size);
    pos = ReplaceAllocation(old, ptr);

    Dump("realloc", pos);

    pthread_mutex_unlock(&mutex);
    return ptr;
}

void __wrap_free(void *ptr)
{
    int pos;
    
    pthread_mutex_lock(&mutex);
    pos = RemoveAllocation(ptr);
    __real_free(ptr);
    
    Dump("free", pos);
    
    pthread_mutex_unlock(&mutex);
}

char *__wrap_strdup(const char *s)
{
    char *ptr;
    int pos;
    
    pthread_mutex_lock(&mutex);
    ptr = __real_strdup(s);
    pos = AddAllocation((void *)ptr);
    
    Dump("strdup", pos);
    
    pthread_mutex_unlock(&mutex);
    return ptr;
}

char *__wrap_strndup(const char *s, size_t n)
{
    char *ptr;
    int pos;
    
    pthread_mutex_lock(&mutex);
    ptr = __real_strndup(s, n);
    pos = AddAllocation((void *)ptr);
    
    Dump("strndup", pos);
    
    pthread_mutex_unlock(&mutex);
    return ptr;
}
