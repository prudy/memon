#
# Copyright (C) 2008 Przemek Rudy
#
# This software may be modified and distributed under the terms
# of the MIT license.  See the LICENSE file for details.
#
 
.PHONY: subonly everything

all: subonly everything

subonly_ver1:
	@echo "Monitor only the memon_test_sub1 functions"
	gcc -c memon_test_sub1.c -o subonly_memon_test_sub1.o
#	objcopy --redefine-sym malloc=__wrap_malloc --redefine-sym realloc=__wrap_realloc --redefine-sym free=__wrap_free --redefine-sym strdup=__wrap_strdup --redefine-sym strndup=__wrap_strndup subonly_memon_test_sub1.o
	gcc -Wl,-r,--wrap=malloc,--wrap=realloc,--wrap=free,--wrap=strdup,--wrap=strndup -nostdlib memon.c subonly_memon_test_sub1.o -o subonly_memon.o
	gcc memon_test.c subonly_memon.o -o subonly

subonly:
	@echo "Monitor only the memon_test_sub1 functions"
	gcc -Wl,-r,--wrap=malloc,--wrap=realloc,--wrap=free,--wrap=strdup,--wrap=strndup -nostdlib memon.c memon_test_sub1.c -o subonly_memon.o
	gcc memon_test.c subonly_memon.o -o subonly

everything:
	@echo "Monitor functions from all files"
	gcc -Wl,--wrap=malloc,--wrap=realloc,--wrap=free,--wrap=strdup,--wrap=strndup memon.c memon_test.c memon_test_sub1.c -o everything

# alternatively use libs:
libmemon:
	gcc -c memon.c
	gcc -Wl,-r,--wrap=malloc,--wrap=realloc,--wrap=free,--wrap=strdup,--wrap=strndup -nostdlib memon.o -o wrap_memon.o
	ar rcs libmemon.a wrap_memon.o
