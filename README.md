# memon

This is easy, light and quickly linkable memory allocations monitor for C code.
```
Output format is:
name @p:a [    ]n

where:
name - allocation function (optional prefix per module)
@p   - memory position of the operation
a    - total allocations
[  ] - graphical representation of memory positions with:
       + - just allocated
       - - just deallocated
       * - occupied by previous allocations
       . or <space> - unallocated
<  ] - grpahical representation showing the initial allocations do not fit the
       screen (MEMON_DUMP_MAX)
n    - number of unknown locations released (e.g. free position not maintained)
overflow - if more allocations were made than can be tracked (MEMON_HISTORY_MAX)
```

By default the screen colouring is enabled, that makes easier column location.
To disable define MEMON_NO_COLORS.
```
Output from examples is as follows:

Test begin
malloc  @0  :  1 [+                                                           ]0 0xa7f010
malloc  @1  :  2 [*+                                                          ]0 0xa7f030
malloc  @2  :  3 [**+                                                         ]0 0xa7f050
strdup  @3  :  4 [***+                                                        ]0 0xa7f070
strndup @4  :  5 [****+                                                       ]0 0xa7f090
Sub1 begin
malloc  @5  :  6 [*****+                                                      ]0 0xa7f0b0
malloc  @6  :  7 [******+                                                     ]0 0xa7f0d0
malloc  @7  :  8 [*******+                                                    ]0 0xa7f0f0
free    @0  :  7 [-*******                                                    ]0 0xa7f010
free    @6  :  6 [ *****-*                                                    ]0 0xa7f0d0
strdup  @0  :  7 [+***** *                                                    ]0 0xa7f0d0
strndup @6  :  8 [******+*                                                    ]0 0xa7f010
free    @7  :  7 [*******-                                                    ]0 0xa7f0f0
free    @5  :  6 [*****-*                                                     ]0 0xa7f0b0
free    @6  :  5 [***** -                                                     ]0 0xa7f010
free    @0  :  4 [-****                                                       ]0 0xa7f0d0
Sub1 end
realloc @2  :  4 [ *+**                                                       ]0 0xa7f050
free    @1  :  3 [ -***                                                       ]0 0xa7f030
free    @2  :  2 [  -**                                                       ]0 0xa7f050
free    @4  :  1 [   *-                                                       ]0 0xa7f090
free    @3  :  0 [   -                                                        ]0 0xa7f070
Test end
```

